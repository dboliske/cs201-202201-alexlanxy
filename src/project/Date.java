// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/16 	Program Name: Date
// This class is used to solve the date problem of the produce item.
package project;

import java.util.regex.Pattern;

public class Date {
    private int year;   // year of the date instance
    private Month month;  // month of the date instance
    private int day;    // day of the date instance

    // regular expression
    private static String datePattern = "([1-9]|[0][1-9]|[1][0-2])(-|/)"
            + "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
            + "([0-9]+)";

    // default constructor
    public Date() {
        year = 1970;
        month = Month.JANUARY;
        day = 1;
    }

    // non-default constructor
    public Date(int year, Month month, int day) {
        this();
        setYear(year);
        setMonth(month);
        setDay(day);
    }

    // constructor to set String
    public Date(String d) {
        this();
        if (Pattern.matches(datePattern, d)) {
            String[] values = d.split("-|/");
            setYear(Integer.parseInt(values[2]));
            setMonth(Integer.parseInt(values[1]));
            setDay(Integer.parseInt(values[0]));
        }
    }

    // accessor method for year
    public int getYear() {
        return year;
    }

    // mutator method for year
    public void setYear(int year) {
        this.year = year;
    }

    // accessor method for month
    public Month getMonth() {
        return month;
    }

    // mutator method for month
    public void setMonth(Month month) {
        this.month = month;
    }

    // mutator method for month by int
    public void setMonth(int month) {
        Month m = Month.getMonth(month);
        if (m != null){
            this.month = m;
        }
    }

    // accessor method for day
    public int getDay() {
        return day;
    }

    // mutator method for day
    public void setDay(int day) {
        if (day >= 1 && day <= month.getDays()) {
            this.day = day;
        }
    }

    // equals method
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Date)){
            return false;
        }
        Date date = (Date) obj;
        return this.year == date.year && this.month == date.month && this.day == date.day;
    }

    // toString method
    @Override
    public String toString() {
        return day + "/" + month.getNum() + "/" + year;
    }



}
