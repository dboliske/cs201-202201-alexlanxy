// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/16 	Program Name: App
// This class is an application class.
package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class App {
    // main method
    public static void main(String[] args) {
        String filename = "src/project/stock.csv";
        ArrayList<Item> items = new ArrayList<>();  // stock array list
        ArrayList<ItemType> itemTypes = new ArrayList<>();

        boolean finishReading = readFile(filename, items, itemTypes);
        if (finishReading) {
            menu(items, itemTypes, filename);
        }

    }

    // static variable: datePattern. Used to identify if a variable is a date.
    public static String datePattern = "([1-9]|[0][1-9]|[1][0-2])(-|/)"
            + "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
            + "([0-9]+)";

    // read a csv file by file name and pick out the item types, then put the data in an array list of items
    public static boolean readFile(String filename, ArrayList<Item> items, ArrayList<ItemType> itemTypes ) {
        try {
            File f = new File(filename);
            Scanner input = new Scanner(f);
            while (input.hasNextLine()) {
                String[] value = input.nextLine().split(",");
                double price = Double.parseDouble(value[1]);
                // pick out the item type
                ItemType itemType = new ItemType(value[0], price);

                // add the itemType to itemTypes:
                //      If itemType has existed, return the index.
                //      Else, add the itemType and return the index.
                int index = addItemType(itemTypes,itemType);

                // identify the subclass of the item, then new an instance and add it to the stock array list.
                if (value.length == 2) {
                    Item item = new ShelvedItem(itemTypes.get(index));
                    items.add(item);
                } else if (value.length == 3) {
                    if (Pattern.matches(datePattern, value[2])) {
                        Date d = new Date(value[2]);
                        Item item = new ProduceItem(itemTypes.get(index), d);
                        items.add(item);
                    } else {
                        int ageRes = Integer.parseInt(value[2]);
                        Item item = new AgeRestrictedItem(itemTypes.get(index), ageRes);
                        items.add(item);
                    }
                }
            }
            input.close();
        } catch (FileNotFoundException fnf) {
            System.out.println("The stock file is not found.");
            return false;
        } catch (Exception e) {
            System.out.println("An error occurred reading in file.");
            return false;
        }
        return true;

    }

    // save the stock array list to a file.
    public static void saveFile(String filename,ArrayList<Item> items) {
        try {
            FileWriter writer = new FileWriter(filename);
            for (Item item : items){
                writer.write(item.toCSV() + "\n");
                writer.flush();
            }
            System.out.println("Save successfully.");

        } catch (Exception e) {
            System.out.println("An error occurred saving to file.");
        }
    }

    // add the itemType to itemTypes:
    //      If itemType has existed, return the index.
    //      Else, add the itemType and return the index.
    public static int addItemType(ArrayList<ItemType> itemTypes, ItemType itemType) {
        int index = searchItemType(itemTypes,itemType);
        if (index == -1) {
            itemTypes.add(itemType);
            index = itemTypes.size() - 1;
        }
        return index;
    }

    // search itemType:
    //  if itemType exists, return the index
    //  else, return -1
    public static int searchItemType(ArrayList<ItemType> itemTypes, ItemType itemType) {
        for (int i = 0; i < itemTypes.size(); i++) {
            if (itemTypes.get(i).equals(itemType)) {
                return i;
            }
        }
        return -1;
    }

    // search the items of the subclass:
    //      1: ProduceItem  2: ShelvedItem 3: AgeRestrictedItem
    public static ArrayList<Item> searchSubItems(ArrayList<Item> items, String sub) {
        ArrayList<Item> existList = new ArrayList<>();
        switch (sub) {
            case "1":
                for (Item item: items) {
                    if (item instanceof ProduceItem) {
                        existList.add(item);
                    }
                }
                break;
            case "2":
                for (Item item: items) {
                    if (item instanceof ShelvedItem) {
                        existList.add(item);
                    }
                }
                break;
            case "3":
                for (Item item: items) {
                    if (item instanceof AgeRestrictedItem) {
                        existList.add(item);
                    }
                }
                break;
        }
        return existList;
    }

    // search items by name, return a list
    public static ArrayList<Item> searchItemsByName(ArrayList<Item> items, String name) {
        ArrayList<Item> existList = new ArrayList<>();
        for (Item item: items) {
            if (item.getItemType().getName().equals(name)) {
                existList.add(item);
            }
        }
        return existList;
    }


    // main menu
    public static void menu(ArrayList<Item> items, ArrayList<ItemType> itemTypes, String filename) {
        Scanner input = new Scanner(System.in);
        boolean done = false;
        do {
            System.out.println("===============================");

            System.out.println("Menu:");
            System.out.println("1. Add items to the store.");
            System.out.println("2. Search for items by name.");
            System.out.println("3. List all the items.");
            System.out.println("4. Remove items from the store.");
            System.out.println("5. Modify name and price.");
            System.out.println("6. Select items to the cart and sell.");
            System.out.println("7. Save to the file.");
            System.out.println("8. Exit without saving.");

            System.out.println("===============================");


            System.out.print("Your choice: ");
            String in = input.nextLine();
            switch (in) {
                case "1":
                    userAddItem(items, itemTypes, input);
                    break;
                case "2":
                    userSearchItemsByName(items,input);
                    break;
                case "3":
                    printList(items);
                    break;
                case "4":
                    removeItem(items, input);
                    refreshItemTypes(items, itemTypes);
                    break;
                case "5":
                    modifyItemType(itemTypes, input);
                    break;
                case "6":
                    selectAndSell(items,input);
                    // after select items to the cart, items will be removed from the stock array list.
                    refreshItemTypes(items, itemTypes);
                    break;
                case "7":
                    saveFile(filename,items);
                    break;
                case "8":
                    done = true;
                    break;
                default:
                    System.out.println("That's not an option.");
            }
        } while (!done);

        input.close();
        System.out.println("GoodBye!");
    }

    // User chooses which subclass to add.
    public static void userAddItem(ArrayList<Item> items, ArrayList<ItemType> itemTypes, Scanner input) {
        boolean done = false;
        do {
            System.out.println("Please choose the type to add:");
            System.out.println("1. Produce Item");
            System.out.println("2. Shelved Item");
            System.out.println("3. Age Restricted Item");
            System.out.println("0. Back");
            System.out.print("Choice: ");
            String in = input.nextLine();

            switch (in) {
                case "1":
                case "2":
                case "3":

                    userAddSubItem(items, itemTypes, input, in);
                    break;
                case "0":
                    done = true;
                    break;
                default:
                    System.out.println("That's not an option.");
            }
        } while (!done);

    }

    // user chooses to add new item or to duplicate an existed one.
    public static void userAddSubItem(ArrayList<Item> items, ArrayList<ItemType> itemTypes, Scanner input, String sub) {
        ArrayList<Item> existList = searchSubItems(items, sub);
        // if item exists, user can choose add or duplicate
        if (existList.size() != 0) {
            printList(existList);
            System.out.println("Those are the items that exist in the store. Add new or duplicate?");
            System.out.println("1. Add new\n2. Add duplicate\n0. Back");
            System.out.print("Choice: ");
            String in = input.nextLine();
            switch (in) {
                case "1":
                    userAddNewItem(items,itemTypes, input, sub);
                    break;
                case "2":
                    userDuplicateItem(items, input, sub);
                    break;
                case "0":
                    break;
                default:
                    System.out.println("That's not a choice.");
            }
            // if item doesn't exist, add an item.
        } else {
            System.out.println("No such item exists, input other information to add.");
            userAddNewItem(items,itemTypes, input, sub);
        }

    }

    // input information to add a new item.
    public static void userAddNewItem(ArrayList<Item> items,ArrayList<ItemType> itemTypes, Scanner input, String sub) {
        // input a name
        System.out.print("name(enter 0 to return): ");
        String name = input.nextLine();
        if (name.equals("0")) {
            return;
        }

        while (true) {
            // input a price
            System.out.print("price(number | enter 0 to return): ");
            String in = input.nextLine();
            if (in.equals("0")){
                return;
            }
            try{
                double price = Double.parseDouble(in);
                ItemType itemType = new ItemType(name, price);
                // input the subclass variable.
                boolean addSuc = userAddItemBySubVariable(items, itemType, input, sub);
                if (addSuc) {
                    addItemType(itemTypes,itemType);
                    System.out.println("Add Successfully!");
                }
                return;
            } catch (Exception e) {
                System.out.println("That's not a number.");
            }
        }


    }

    // input the subclass variable.
    public static boolean userAddItemBySubVariable(ArrayList<Item> items, ItemType itemType, Scanner input, String sub) {
        switch (sub) {
            // ProduceItem
            case "1":
                while(true) {
                    System.out.print("expiration date(mm/dd/yyyy | enter 0 to return): ");
                    String date = input.nextLine();
                    if (Pattern.matches(datePattern, date)) {
                        Date d = new Date(date);
                        ProduceItem produceItem = new ProduceItem(itemType, d);
                        items.add(produceItem);
                        break;
                    } else if (date.equals("0")) {
                        return false;
                    } else {
                        System.out.println("That's not a valid date.");
                    }
                }
                break;
            // ShelvedItem
            case "2":
                ShelvedItem shelvedItem = new ShelvedItem(itemType);
                items.add(shelvedItem);
                break;

            // AgeRestrictedItem
            case "3":
                while(true) {
                    System.out.print("Age restriction(integer | enter 0 to return): ");
                    try{
                        String in = input.nextLine();
                        if (in.equals("0")) {
                            return false;
                        }
                        int age = Integer.parseInt(in);
                        AgeRestrictedItem ageRestrictedItem = new AgeRestrictedItem(itemType, age);
                        items.add(ageRestrictedItem);
                        break;
                    } catch (Exception e) {
                        System.out.println("That's not an integer.");
                    }
                }
                break;
            default:

        }
        return true;
    }

    // duplicate an existed item.
    public static void userDuplicateItem(ArrayList<Item> items, Scanner input, String sub) {
        String prompt = "Choose a serial number to continue to duplicate.\nChoice: ";
        while(true) {
            ArrayList<Item> existList = searchSubItems(items, sub);
            int in = listChoice(existList, input, prompt);
            if (in != -1) {
                Item item;
                int index = in -1;
                if (existList.get(index) instanceof AgeRestrictedItem) {
                    AgeRestrictedItem ageRestrictedItem = (AgeRestrictedItem) existList.get(index);
                    item = new AgeRestrictedItem(ageRestrictedItem.getItemType(), ageRestrictedItem.getAgeRes());
                } else if (existList.get(index) instanceof ProduceItem) {
                    ProduceItem produceItem = (ProduceItem) existList.get(index);
                    item = new ProduceItem(produceItem.getItemType(),produceItem.getExpDate());
                } else {
                    ShelvedItem shelvedItem = (ShelvedItem) existList.get(index);
                    item = new ShelvedItem(shelvedItem.getItemType());
                }
                items.add(item);
                System.out.println("Duplicate successfully.");
            }
            else {
                return;
            }
        }


    }

    // User search items by name
    public static void userSearchItemsByName(ArrayList<Item> items, Scanner input) {
        System.out.print("Input a name to search(enter 0 to return): ");
        String name = input.nextLine();
        if (name.equals("0")) {
            return;
        }
        ArrayList<Item> result = searchItemsByName(items, name);
        if (result.size() != 0) {
            for (Item item: result) {
                System.out.println(item);
            }
        }
        else {
            System.out.println("No such item.");
        }
    }

    // print a list and add the serial number to the front
    public static void printList(ArrayList arrayList) {
        for (int i = 0; i < arrayList.size(); i++)  {
            System.out.println(i + 1 + ". " + arrayList.get(i));
        }
    }

    // modify the name and price of the item, which is the ItemType.
    public static void modifyItemType(ArrayList<ItemType> itemTypes, Scanner input) {
        String prompt = "Choose a serial number to modify.\nChoice: ";
        int in = listChoice(itemTypes, input, prompt);
        if (in == -1) {
            return;
        } else {
            int index = in - 1;
            ItemType itemType = itemTypes.get(index);
            System.out.print("New name(enter 0 to jump over): ");
            String name = input.nextLine();
            if(!name.equals("0")){
                itemType.setName(name);
                System.out.println("Name modified successfully!");
            }
            while(true) {
                System.out.print("New price(enter 0 to return): ");
                try {
                    String priceIn = input.nextLine();
                    if (priceIn.equals("0")) {
                        return;
                    }
                    double price = Double.parseDouble(priceIn);
                    itemType.setPrice(price);
                    System.out.println("Price modified successfully.");
                    return;
                } catch (Exception e) {
                    System.out.println("That's not a number.");
                }
            }


        }
    }

    // If an item is removed, needs to refresh the itemTypes, some itemTypes not existed may need to be removed
    public static void refreshItemTypes(ArrayList<Item> items, ArrayList<ItemType> itemTypes) {
        itemTypes.clear();
        for (Item item: items) {
             addItemType(itemTypes, item.getItemType());
        }

    }

    // List the items/itemTypes that need to be chosen, and prompt the user for a serial number.
    public static int listChoice(ArrayList arrayList, Scanner input, String prompt) {
        printList(arrayList);
        System.out.println("0. Back");

        System.out.print(prompt);
        int in;

        try {
            String inS = input.nextLine();
            if (inS.equals("0")) {
                return -1;
            }
            in = Integer.parseInt(inS);
        } catch (Exception e) {
            System.out.println("That's not an option.");
            return -1;
        }
        if (in < 1 || in > arrayList.size()) {
            System.out.println("That's not an option.");
            return -1;
        } else {
            return in;
        }
    }

    // select items to the cart, and sell
    public static void selectAndSell(ArrayList<Item> items, Scanner input) {
        ArrayList<Item> cart = new ArrayList<>();
        boolean done = false;
        do {
            System.out.println("1. Add items to cart.");
            System.out.println("2. List the items in cart.");
            System.out.println("3. Sell items from cart.");
            System.out.println("0. Put back items from cart and return back.");
            System.out.print("Option: ");
            String in = input.nextLine();
            switch (in) {
                case "1":
                    selectItemCart(items, cart, input);
                    break;
                case "2":
                    if (cart.size() == 0) {
                        System.out.println("The cart is empty.");
                    } else {
                        printList(cart);
                    }
                    break;
                case "3":
                    if (cart.size() == 0) {
                        System.out.println("The cart is empty.");
                    } else {
                        cart.clear();
                        System.out.println("Sell successfully!");
                    }
                    break;
                case "0":
                    for (Item item:cart) {
                        items.add(item);
                    }
                    cart.clear();
                    done = true;

                default:
                    System.out.println("That's not an option.");
            }
        } while(!done);


    }

    // select the items to the cart, it will remove the items from the stock array list.
    public static void selectItemCart(ArrayList<Item> items, ArrayList<Item> cart, Scanner input) {
        boolean done = false;
        do {
            String prompt = "Choose a serial number to continue to add to the cart.\nChoice: ";
            int in = listChoice(items,input, prompt);
            if (in != -1) {
                int index = in - 1;
                cart.add(items.get(index));
                items.remove(index);
                System.out.println("Add to the cart successfully!");
            } else {
                done = true;
            }
        } while (!done);
    }



    // user remove the item by the serial number.
    public static void removeItem(ArrayList<Item> items, Scanner input) {
        boolean done = false;
        do {
            String prompt = "Choose a serial number to continue to remove item.\nChoice: ";
            int in = listChoice(items,input, prompt);
            if (in != -1) {
                int index = in - 1;
                items.remove(index);
                System.out.println("Remove successfully!");
            } else {
                done = true;
            }
        } while (!done);
    }

}
