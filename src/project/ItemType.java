// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/17 	Program Name: ItemType
// ItemType: If items have the same name and price, they have the same itemType. ItemType is an association relation of Item.
package project;

public class ItemType {
    private String name;    // name of the item
    private double price;   // price of the item

    // default constructor
    public ItemType() {
        name = "unamed";
        price = 0.0;
    }

    // non-default constructor
    public ItemType(String name, double price) {
        this();
        setName(name);
        setPrice(price);
    }

    // accessor method of name
    public String getName() {
        return name;
    }

    // mutator method of name
    public void setName(String name) {
        this.name = name;
    }

    // accessor method of price
    public double getPrice() {
        return price;
    }

    // mutator method of price
    public void setPrice(double price) {
        if (price >= 0) {
            this.price = price;
        }
    }

    // compare this instance to another to check if they are equal.
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ItemType)) {
            return false;
        }
        ItemType itemType = (ItemType) obj;
        return this.name.equals(itemType.getName())
                && this.price ==  itemType.getPrice();
    }

    // convert the instance to String
    public String toString() {
        return name + " is " + "$" + price + ". ";
    }
}
