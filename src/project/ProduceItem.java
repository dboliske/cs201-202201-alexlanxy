// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/16 	Program Name:
// This class is a subclass of Item

package project;

public class ProduceItem extends Item{
    private Date expDate;   //expiration date of the item

    // default constructor
    public ProduceItem() {
        super();
        this.expDate = new Date();
    }

    // non-default constructor
    public ProduceItem(ItemType itemType, Date expDate) {
        super(itemType);
        this.expDate = new Date();
        setExpDate(expDate);
    }

    // accessor method for expDate
    public Date getExpDate() {
        return expDate;
    }

    // mutator methid for expDate
    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    // compare this instance to another to check if they are equal.
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ProduceItem)){
            return false;
        }
        ProduceItem produceItem = (ProduceItem) obj;
        return super.equals(obj) && this.expDate.equals(produceItem.getExpDate());
    }

    // convert the instance to string
    @Override
    public String toString() {
        return super.toString() + super.getItemType().getName() + " is expired " + "at " + expDate.toString()+ ".";
    }

    // convert the instance to CSV format
    @Override
    public String toCSV() {
        return super.toCSV() + "," + expDate.toString();
    }




}
