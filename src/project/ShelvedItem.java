// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/16 	Program Name: ShelvedItem
// This class is a subclass of Item

package project;

public class ShelvedItem extends Item{
    // same variables as Item

    // default constructor
    public ShelvedItem() {
        super();
    }

    // non-default constructor
    public ShelvedItem(ItemType itemType) {
        super(itemType);
    }

    // compare this instance to another to check if they are equal.
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ShelvedItem)) {
            return false;
        }
        return super.equals(obj);
    }


}
