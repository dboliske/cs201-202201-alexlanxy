// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/16 	Program Name: AgeRestrictedItem
// This class is a subclass of Item

package project;

public class AgeRestrictedItem extends Item{
    private int ageRes; // restriction based on the age of the customer

    // default constructor
    public AgeRestrictedItem() {
        super();
        this.ageRes = 18;
    }

    // non-default constructor
    public AgeRestrictedItem(ItemType itemType, int ageRes) {
        super(itemType);
        this.ageRes = 18;
        this.setAgeRes(ageRes);
    }

    // accessor method for ageRes
    public int getAgeRes() {
        return ageRes;
    }

    // mutator method for ageRes
    public void setAgeRes(int ageRes) {
        if (ageRes > 0) {
            this.ageRes = ageRes;
        }

    }

    // compare this instance to another to check if they are equal.
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AgeRestrictedItem)) {
            return false;
        }
        AgeRestrictedItem ari = (AgeRestrictedItem) obj;
        return super.equals(obj) && this.ageRes == ari.getAgeRes();
    }

    // override the toString method
    @Override
    public String toString() {
        return super.toString() + super.getItemType().getName()+ " has a age restriction of " + ageRes;
    }

    // // override the toCSV method
    @Override
    protected String toCSV() {
        return super.toCSV() + "," + this.getAgeRes();
    }
}
