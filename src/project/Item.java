// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/16 	Program Name: Item
// This class is a superclass Item

package project;

public class Item {

    private ItemType itemType;  // item type of the item, eg. pen,pencil...

    // default constructor
    public Item() {
        itemType = new ItemType();
    }

    // non-default constructor
    public Item(ItemType itemType) {
        this();
        setItemType(itemType);

    }

    // accessor method for name
    public ItemType getItemType() {
        return itemType;
    }

    // mutator method for price
    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    // compare this instance to another to check if they are equal.
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Item)) {
            return false;
        }
        Item item = (Item)obj;
        return this.itemType.equals(item.getItemType());
    }

    // toString method
    @Override
    public String toString() {
        return itemType.toString();
    }

    // convert instance to csv data
    protected String toCSV() {
        return itemType.getName() + "," + itemType.getPrice();
    }

}
