// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Feb 4, 2022 	Program Name: 

/** Test
 * 			input				desired output							actual output							pass or not pass
 * 		30,40,50,-1				40										40.0									pass
 * 		-1						You haven't enter any grade. Exit!		You haven't enter any grade. Exit!		pass
 * 		63.7,30.8,90.6,-1		61.7									61.699999999999996						pass
 * 		1,3,4,6,7,15,-1			6										6.0										pass
 * 
 */

package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		double sum = 0;
		double in = 0;
		int num = 0;
		
		// prompt the user
		System.out.println("Input the grades to compute the average(enter -1 to finish entering).");
		do {
		
			System.out.print("Grade " + (num+1) + ": ");
		
			in = Double.parseDouble(input.nextLine());
			
			// if the user input is -1, skip, and exit the loop.
			if (in != -1) {
				sum += in;
				num ++;
			}
			
		} while (in != -1);
		
		// 0 cannot be the denominator
		if (num == 0) {
			System.out.print("You haven't enter any grade. Exit!");
		} else {
			System.out.print("The average: " + (sum/num));
		}
		
		
		input.close();
	}

}
