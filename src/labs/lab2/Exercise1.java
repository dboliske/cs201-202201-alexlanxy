// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Feb 4, 2022  	Program Name: Input a number for a square
package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user
		System.out.print("Size of the square: ");
		
		int n = Integer.parseInt(input.nextLine());
		
		for (int i=1; i<=n; i++) {
			for (int j=1; j<=n; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		
		input.close();

	}

}
