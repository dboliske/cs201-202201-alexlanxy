// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Feb 4, 2022 	Program Name: Menu
package labs.lab2;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		boolean exit = false;
		String choice = "";
		
		do {
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			
			choice =  input.nextLine();
			
			switch (choice) {
				case "1":
					System.out.println("Hello");
					break;
				case "2":
					System.out.println("Please enter 2 numbers.");
					System.out.print("The first number: ");
					double firstNum = Double.parseDouble(input.nextLine());
					System.out.print("The second number: ");
					double secondNum = Double.parseDouble(input.nextLine());
					System.out.println("Sum: " + (firstNum + secondNum));
					break;
				case "3":
					System.out.println("Please enter 2 numbers.");
					System.out.print("The first number: ");
					firstNum = Double.parseDouble(input.nextLine());
					System.out.print("The second number: ");
					secondNum = Double.parseDouble(input.nextLine());
					System.out.println("Product: " + (firstNum * secondNum));
					break;
				case "4":
					exit = true;   //exit = true, exit the loop.
					break;
				default:
					System.out.println("That's not an option.");
					
					
			}
			
		} while(!exit);
		
		System.out.println("Goodbye!");
		
		input.close();

	}

}
