// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：Feb 15, 2022 	Program Name: Potion.java
package labs.lab4;

public class Potion {
	// create two instance
	private String name;
	private double strength;
	
	// default constructor
	public Potion() {
		this.name = "unnamed";
		this.strength = 0.0;
	}
	
	// non-default constructor
	public Potion(String name, double strength) {
		this.name = name;
		this.strength = 0.0;
		setStrength(strength);		// use setStrength to validate strength
	}
	
	// accessor for name
	public String getName() {
		return name;
	}
	
	// accessor for strength
	public double getStrength() {
		return strength;
	}
	
	// mutator for name
	public void setName(String name) {
		this.name = name;
	}
	
	// mutator for strength
	public void setStrength(double strength) {
		if(validStrength(strength)) {	// use validStrength to validate strength
			this.strength = strength;
		}
		
	}
	
	// to String method
	public String toString() {
		return name + ": " + strength;
	}
	
	// validate the strength
	public boolean validStrength(double strength) {
		return strength >= 0 && strength <= 10;
	}
	
	// compare this potion to the another
	public boolean equals(Potion p) {
		return name.equals(p.name) && strength == p.strength;	// either not equal will return false
	}
	
}



