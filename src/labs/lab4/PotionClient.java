// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：Feb 15, 2022 	Program Name: PotionClient.java
package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion p1 = new Potion();	// instantiate p1 with default construction
		Potion p2 = new Potion("Tonic of Bloodlust", 8);	// instantiate p2 with non-default construction
		
		System.out.println(p1.toString());		// display p1
		System.out.println(p2.toString());		// display p2
		
	}

}
