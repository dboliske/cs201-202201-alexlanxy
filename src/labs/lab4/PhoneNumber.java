// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：Feb 13, 2022 	Program Name: PhoneNumber.java
package labs.lab4;

public class PhoneNumber {
	// create 3 string instance variables
	private String countryCode;
	private String areaCode;
	private String number;
	
	// default constructor
	public PhoneNumber() {
		this.countryCode = "0";
		this.areaCode = "000";
		this.number = "0000000";
	}
	
	// non-default constructor
	public PhoneNumber(String cCode, String aCode, String number) {
		this.countryCode = cCode;
		this.areaCode = "000";
		setAreaCode(aCode);
		this.number = "0000000";
		setNumber(number);
	}
	
	// accessor method for countryCode
	public String getCountryCode() {
		return countryCode;
	}
	
	// accessor method for areaCode
	public String getAreaCode() {
		return areaCode;
	}
	
	// accessor method for number
	public String getNumber() {
		return number;
	}
	
	// mutator method for countryCode
	public void setCountryCode(String cCode) {
		this.countryCode = cCode;
	}
	
	// mutator method for areaCode
	public void setAreaCode(String aCode) {
		if (validAreaCode(aCode)) {		// use validAreaCode method to validate the input aCode, not set if invalid.
			this.areaCode = aCode;
		}
	}
	
	// mutator method for number
	public void setNumber(String number) { 
		if (validNumber(number)) {		// use validNumber method to validate the input number, not set if invalid.
			this.number = number;
		}
		
	}
	
	// toString method
	public String toString() {
		return "+" + countryCode +" (" + areaCode + ") " + number ;
	}
	
	// validate the AreaCode
	public boolean validAreaCode(String aCode) {
		return aCode.length() == 3;
	}
	
	// validate the number
	public boolean validNumber(String number) {
		return number.length() == 7;
	}
	
	// compare this phone number to another
	public boolean equals(PhoneNumber pn) {
		return countryCode.equals(pn.countryCode) // should use .equals to compare two string
			&& areaCode.equals(pn.areaCode)
			&& number.equals(pn.number);
	}
	
	
}
