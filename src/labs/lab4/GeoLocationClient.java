// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：Feb 13, 2022 	Program Name: GeoLocationClient.java
package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation gl1 = new GeoLocation();					// instantiate GeoLocation with default constructor
		GeoLocation gl2 = new GeoLocation(-30, 80);				// instantiate GeoLocation with non-default constructor
		System.out.println("GL1(default): " + gl1.toString());	// display the first instance
		System.out.println("GL2: " + gl2.toString());			// display the second instance
	}

}