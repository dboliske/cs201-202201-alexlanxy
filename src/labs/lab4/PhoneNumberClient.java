// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：Feb 14, 2022 	Program Name: PhoneNumberClient.java
package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		
		PhoneNumber pn1 = new PhoneNumber();						// instantiate the first phone number with the default constructor
		PhoneNumber pn2 = new PhoneNumber("1", "818", "8511273");	// instantiate the second phone number with the non-default constructor
		
		System.out.println(pn1.toString());		// use toString method to display the first instance
		System.out.println(pn2.toString());		// use toString method to display the second instance
	}

}
