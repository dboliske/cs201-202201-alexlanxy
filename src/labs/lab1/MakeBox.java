// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Jan 27, 2022 	Program Name: Make Box
package labs.lab1;

import java.util.Scanner;

public class MakeBox {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt user
		System.out.println("To make a box, you need to input the length, width and depth to calculate the amount of the wood.");
		System.out.print("Length in inches: ");
		double length = Double.parseDouble(input.nextLine()) / 12;  // "/ 12" convert inches to feet  
		
		System.out.print("Width in inches: ");
		double width = Double.parseDouble(input.nextLine()) / 12;	// "/ 12" convert inches to feet 
		
		System.out.print("Depth in inches");
		double depth = Double.parseDouble(input.nextLine()) / 12;	// "/ 12" convert inches to feet 
		
		System.out.println("To make the box, the amount of the wood is = " + ((length*width + length*depth + width*depth) * 2) + " square feet."); 					
		
		
		input.close();
	}

}

/**
 * Test Plan and Result
 * 		
 * 		Length		Width		Depth 	   	Desired Amount(square feet)		Actual Output Amount(square feet)		Pass or Not Pass
 * 		10			20			30			15.28							15.277777777777779						Pass
 * 		15.7		26.9		37.68		28.16							28.159694444444447						Pass
 * 		100			200			300			1527.78							1527.7777777777778						Pass
 * 
 * The program works as expected.
 */
