// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Jan 27, 2022 	Program Name: Inches to Centermeters
package labs.lab1;

import java.util.Scanner;

public class InchesToCms {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user
		System.out.print("This program will convert inches to centimeters, please input a value in inches: ");
		double value = Double.parseDouble(input.nextLine());
		
		// convert the value and display
		System.out.println("The value is " + (value * 2.54) + "centimeters");
		
		input.close();
	}

}

/**
 * Test Plan and Result
 * 		
 * 		Input Inches		Desired Cms 		Actual Output Cms		Pass or Not Pass
 * 		30					76.2				76.2					Pass
 * 		365					927.1				927.1					Pass
 * 		6666				16931.64			16931.64				Pass
 * 
 * 
 * The program works as expected.
 */