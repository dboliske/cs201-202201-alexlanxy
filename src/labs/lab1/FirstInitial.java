// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Jan 27, 2022 	Program Name: First Initial
package labs.lab1;

import java.util.Scanner;

public class FirstInitial {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		System.out.print("Please input your first name: ");
		char firstInitial = input.nextLine().charAt(0);
		
		System.out.println("Your first initial is " + firstInitial);
		
		
		input.close();
	}

}
 