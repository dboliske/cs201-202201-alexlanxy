// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Jan 26, 2022 	Program Name:Arithmetic Calculations
package labs.lab1;

public class ArithmeticCalculations {

	public static void main(String[] args) {
		int myAge = 30;
		int fatherAge = 58;
		int birthYear = 1991;
		float inchHeight = 68.4f;
		
		// My age subtracted from my father's age
		System.out.println("My age subtracted from my father's age is " + (fatherAge - myAge));
		
		// My birth year multiplied by 2
		System.out.println("My birth year multiplied by 2 is " + (birthYear * 2));
		
		// Convert my height in inches to cms
		System.out.println("My height in cms is " + (inchHeight * 2.54));
		
		// Convert my height in inches to feet and inches
		System.out.println("My height is " + ((int)inchHeight / 12) + " feet and " + ((int)inchHeight % 12) + " inches");                           
 		
	
	}

}
