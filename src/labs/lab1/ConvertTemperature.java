// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Jan 27, 2022 	Program Name: Convert Temperature

package labs.lab1;

import java.util.Scanner;

public class ConvertTemperature {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user for a temperature in Fahrenheit
		System.out.print("To convert to Celsius, please input the temperature in Fahrenheit: ");
		double tFah = Double.parseDouble(input.nextLine());
		
		// convert Fahrenheit to Celsius
		System.out.println("The temperature in Celsius is " + ((tFah - 32) / 1.8));
		
		System.out.println("");
		
		// prompt the user for a temperature in Celsius
		System.out.print("To convert to Fahrenheit, please input the temperature in Ceisius: ");
		double tCel = Double.parseDouble(input.nextLine());
		
		// convert Celsius to Fahrenheit
		System.out.print("The temperature in Fahrenheit is " + (tCel * 1.8 + 32));
		
		input.close();

	}

}

/**
 * Test Plan and Result
 * 		
 * 		1.  Test Fahrenheit to Celsius
 * 
 * 						Plan											Result
 * 		--------------------------------------		---------------------------------------------
 * 		Input Fahrenheit	Desired Celsius   	 	Actual Output Fahrenheit	Pass or Not Pass
 * 		300					148.89					148.88888888888889			Pass
 * 		50					10						10.0						Pass
 * 		-200				-128.89					-128.88888888888889			Pass
 * 		
 * 		The program works as expected.
 * 
 * 		=========================================================================================
 * 
 * 		2. Test Celsius to Fahrenheit
 * 
 * 						Plan											Result
 * 		--------------------------------------		---------------------------------------------
 * 		Input Celsius		Desired Fahrenheit   	 Actual Output Fahrenheit	Pass or Not Pass
 * 		875					1607					1607.0						Pass
 * 		20					68						68.0						Pass
 * 		-250				-418					-418.0						Pass
 * 
 * 
 * 
 * 		The program works as expected.
 */





