// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Jan 26, 2022	Program Name: Prompt and Echo Name
package labs.lab1;

import java.util.Scanner;

public class PromptNameEcho {

	public static void main(String[] args) {
		//create a scanner
		Scanner input = new Scanner(System.in);	
		
		// prompt the user for a name
		System.out.print("Please in put your name: "); 
		String name = input.nextLine();
		
		// echo the name
		System.out.println("Your name is: " + name);
		
		input.close();
	}

}
