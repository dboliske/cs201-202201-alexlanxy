// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/3/20 	Program Name: QueueAppication
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class QueueApplication {
    public static void main(String[] args) {
        // create a scanner
        Scanner input = new Scanner(System.in);
        ArrayList<String> customers = new ArrayList<String>();
        String[] menu = {"Add customer to queue", "Help customer", "Exit"};
        boolean done = false;
        // print the menu
        do {
            for (int index = 0; index < menu.length; index++) {
                System.out.println((index + 1) + ". " + menu[index]);
            }
            System.out.print("Choice: ");
            String in = input.nextLine();
            switch (in) {
                case "1":
                    addCustomer(customers, input);
                    break;
                case "2":
                    helpCustomer(customers);
                    break;
                case "3":
                    done = true;
                    break;
                default:
                    System.out.println("That's not an option.");
            }

            } while (!done);


        input.close();

        System.out.println("Goodbye!");
    }

    // method to add customer
    public static void addCustomer(ArrayList<String> customers, Scanner input) {
        System.out.print("Please input the customer's name: ");
        customers.add(input.nextLine());
    }

    // method to help customer(remove the front customer)
    public static void helpCustomer(ArrayList<String> customers) {
        if (customers.size() == 0) {
            System.out.println("There's no customer to help!");
        } else {
            String helped =  customers.remove(0);
            System.out.println(helped + " is removed.");
        }


    }
}
