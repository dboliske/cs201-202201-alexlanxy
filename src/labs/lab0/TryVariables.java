// Name: Xinyu Lan(兰新宇)   Course: cs201  sec. #0    Program Name: TryVariables      Date:Jan.23, 2022
package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		short small = 25;		//This variable should be declared.   
		byte tiny = 19;

		float f = .0925F;		//Missed the semicolon.		
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small);   // First letter of "System" should be capital.
		System.out.println("tiny is " + tiny);     // This variable should be the same as declared.
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal); // Replace the ',' with '.'
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is " + character);//Missed '+'
		System.out.println("t is " + t);

	}

}