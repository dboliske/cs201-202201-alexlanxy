// Name: Xinyu Lan(兰新宇)   Course: cs201  sec. #0    Program Name: Square      Date:Jan.23, 2022

/*  Pseudocode
1. I want to make a 10*10 filled Square using the char "a" as dot;
2. I will make the program print “a” 10 times in the first line, and then print a newline;
3. And then the program will repeat printing "a" 10 times and a newline;
4. When the program has already printed ten lines, it will end.
*/


package labs.lab0;

public class Square {

	public static void main(String[] args) {
		char a = 'a';
		for (int row = 1; row <= 10; row++) {			//Repeat printing line 10 times.
			for(int col = 1; col <= 10; col++) {      	//Every line, "a" should be printed 10 times.
				System.out.print(a);
			}
			System.out.println("");                 	//To print a newline, I use the "println".
		}		
	}
}

//I've got the desired result. 


