// Name: Xinyu Lan(兰新宇)   Course: cs201  sec. #0    Program Name: NameAndBirthdate      Date:Jan.23, 2022    


package labs.lab0;

public class NameAndBirthdate {

	public static void main(String[] args) {
		String name = "Xinyu Lan";
		String birthMonth = "Dec.";
		String birthDay = "28";
		String birthYear = "1991";
		
		System.out.println("My name is " + name + " and my birthdate is " + birthMonth + " " + birthDay + ", " + birthYear + ".");
	}

}
