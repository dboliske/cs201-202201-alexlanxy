// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/4 	Program Name: Insertion Sort
package labs.lab7;

public class Exercise2 {
    public static String[] sort(String[] array) {
        // from the second one
        for (int i = 1; i < array.length; i++) {
            // Insert the current word into the front
            for(int j = i; j > 0 && array[j].compareTo(array[j-1]) < 0; j--) {
                String temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
            }
        }
        return array;
    }


    public static void main(String[] args) {
        String[] words = {"cat", "fat", "dog", "apple", "bat", "egg"};
        words = sort(words);
        for (String word : words) {
            System.out.print(word + " ");
        }


    }
}
