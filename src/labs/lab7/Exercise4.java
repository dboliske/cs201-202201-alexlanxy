// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/4 	Program Name: Binary Search
package labs.lab7;

import java.util.Scanner;

public class Exercise4 {
    public static int search(String[] array, String value, int start, int end) {
        // out of range.
        if (value.compareTo(array[start]) < 0 || value.compareTo(array[end]) > 0) {
            return -1;
        }

        int mid = (start + end) / 2;
        if (array[mid].compareTo(value) > 0) {
            return search(array, value, start, mid);
        } else if (array[mid].equals(value)){
            return mid;
        } else {
            return search(array, value, mid + 1, end);
        }
    }

    public static int search(String[] array, String value) {
        return search(array, value, 0, array.length - 1);
    }

    public static void main(String[] args) {
        String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
        Scanner input = new Scanner(System.in);
        System.out.print("Search: ");
        String in = input.nextLine();
        int index = search(lang, in);
        if (index == -1) {
            System.out.println("Not found.");
        } else {
            System.out.println("Found at index " + index + ".");
        }
        input.close();
    }
}
