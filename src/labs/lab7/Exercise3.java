// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/4 	Program Name: Selection Sort
package labs.lab7;

public class Exercise3 {
    public static double[] sort(double[] array) {
        for (int i = 0; i <array.length - 1; i++) {
            int minIndex = i;
            // find the index of min
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            // swap the min and the first
            if (minIndex != i) {
                double temp = array[minIndex];
                array[minIndex] = array[i];
                array[i] = temp;
            }

        }
        return array;
    }
    public static void main(String[] args) {
        double[] doubles = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        doubles = sort(doubles);
        for (double d : doubles) {
            System.out.print(d + " ");
        }
    }
}
