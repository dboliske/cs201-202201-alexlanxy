// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/4 	Program Name: BubbleSort
package labs.lab7;

public class Exercise1 {
    public static int[] sort(int[] array) {
        boolean done;
        do {
            done = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i+1]) {
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    done = false;
                }
            }
        } while (!done);

        return array;
    }

    public static void main(String[] args) {
        int[] nums = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        nums = sort(nums);
        for (int num:nums) {
            System.out.print(num + " ");
        }
    }
}
