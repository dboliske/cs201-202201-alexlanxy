// Name: Xinyu Lan(兰新宇)	Course: CS201	Date：Feb 5, 2022 	Program Name: Find the minimum
package labs.lab3;

public class Exercise3 {

	public static void main(String[] args) {
		// declare nums and initialize nums with the given array
		int[] nums = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		// assuming the first one is the minimum.
		int min = nums[0];
		for (int i=1; i<nums.length; i++) {
			if (nums[i] < min) { 	// compare the minimum with the next element, if the next element is smaller, update min with it.
				min = nums[i];		
			}
		}
		System.out.println("Min: " + min);	// print the minimum
	}

}
