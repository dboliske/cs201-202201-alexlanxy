// Name: Xinyu Lan(兰新宇)	Course: CS201  		Date：Feb 4, 2022 	Program Name: Compute the average grade
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) throws IOException {
		// create file object
		File file = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner(file);
		int count = 0;
		double[] grades = new double[2];
		// read the lines until the end.
		while(input.hasNextLine()) {
			String[] values = input.nextLine().split(",");
			// if the array is full, resize the array
			if (count == grades.length) {
				double[] bigger = new double[grades.length * 2];	// initialize a bigger array in which copy all the elements of grades
				for (int i=0; i<grades.length; i++) {
					bigger[i] = grades[i];
				}
				grades = bigger;	// make grades point to the address of the resized array
				bigger = null;
			}
			grades[count] = Double.parseDouble(values[1]);
			count++;
		}
		
		input.close();
		
		// trim array down
		double[] smaller = new double[count];	// after the last "count++", the count now is the lines of the file. 
		for (int i=0; i<smaller.length; i++) {
			smaller[i] = grades[i];
		}
		grades = smaller;
		smaller = null;
		
		// compute the sum of the grades
		double sum = 0;
		for (int i=0; i<grades.length; i++) {
			sum = sum + grades[i];
		}
		// compute the average grade
		double average = sum/grades.length;
		System.out.print("Average: " + average);
	}

}
