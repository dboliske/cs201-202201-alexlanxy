// Name: Xinyu Lan(兰新宇)	Course: CS201	Date：Feb 5, 2022 	Program Name: Input and save file
package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		int[] nums = new int[1];
		String in;
		int count = 0;
		System.out.println("Input numbers to save, enter 'done' to finish");
		do {
			// prompt the user for numbers
			System.out.print("Input: ");
			in = input.nextLine();
			if (!in.equals("done")) {
				// if the array is full, resize the array
				if(count == nums.length) {
					int[] bigger = new int[2 * nums.length];
					for(int i = 0; i < nums.length; i++) {
						bigger[i] = nums[i];
					}
					nums = bigger;	// make nums point to the address of the resized array
					bigger = null;
				}
				// count is also the index of the first unused element.
				nums[count] = Integer.parseInt(in);
				count ++;
			}
		} while (!in.equals("done"));
		
		
		
		// trim the array down
		int[] smaller = new int[count];		// after the last "count++", the count now is the times of user input.
		for(int i = 0; i < count; i++) {
			smaller[i] = nums[i];
		}
		nums = smaller;
		smaller = null;
		
		// prompt the user for a filename
		System.out.print("Enter a file name to save: ");
		String fileName = input.nextLine();
		
		try {
			FileWriter f = new FileWriter(fileName);
			for(int i = 0; i < nums.length; i++) {
				f.write(nums[i] + "\n");	// write every input of the user into every line.
			}
			f.flush();	// flush the current OutputStream object.
			f.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		input.close();
		System.out.println("Saved Success!");
	}

}
