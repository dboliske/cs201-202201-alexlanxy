// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/3/6 	Program Name:
package labs.lab5;

public class CTAStation extends GeoLocation {
    // create instance variables
    private String name;
    private String location;
    private boolean wheelchair;
    private boolean open;

    public CTAStation() {
        super();
        this.name = "unnamed";
        this.location = "";
        this.wheelchair = false;
        this.open = false;
    }

    public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open ) {
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelchair = wheelchair;
        this.open = open;

    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public boolean hasWheelchair() {
        return this.wheelchair;
    }

    public boolean isOpen() {
        return this.open;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public String toString() {
        return name + " is " + location + ", and the geographical location is at " + super.toString() + ".";
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof CTAStation)) {
            return false;
        }
        CTAStation ctaStation = (CTAStation) obj;
        return this.name.equals(ctaStation.getName()) &&
                this.location.equals(ctaStation.getLocation()) &&
                this.wheelchair == ctaStation.hasWheelchair() &&
                this.open == ctaStation.isOpen();
    }
}
