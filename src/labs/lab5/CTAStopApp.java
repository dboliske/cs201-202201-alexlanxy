// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/3/6 	Program Name:
package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {
    public static void main(String[] args)  {
        Scanner input = new Scanner(System.in);
        CTAStation[] stations = readFile("src/labs/lab5/CTAStops.csv");
        menu(stations);
        input.close();
        System.out.println("Goodbye!");
    }

    public static CTAStation[] readFile(String filename) {
        CTAStation[] stations = new CTAStation[10];
        int count = -1;
        try {
            File f = new File(filename);
            Scanner input = new Scanner(f);
            while (input.hasNextLine()) {
                try {
                    String[] value = input.nextLine().split(",");
                    if (count >= 0) {
                        if (count == stations.length) {
                            stations = resize(stations, stations.length * 2);
                        }

                        stations[count] = new CTAStation(
                                            value[0],
                                            Double.parseDouble(value[1]),
                                            Double.parseDouble(value[2]),
                                            value[3],
                                            Boolean.parseBoolean(value[4]),
                                            Boolean.parseBoolean(value[5])
                                            );
                    }
                    count++;
                } catch (Exception e) {
                    System.out.println("The data format of line " + (count+2) +" in csv file is incorrect");
                }

            }


            input.close();
        } catch (FileNotFoundException fnf) {
            System.out.println("File not found!");
        } catch (Exception e) {
            System.out.println("An error occurred reading in file!");
        }

        stations = resize(stations, count);


        return stations;

    }

    public static CTAStation[] resize(CTAStation[] stations, int size) {
        CTAStation[] temp = new CTAStation[size];
        int limit = stations.length > size ? size : stations.length;
        for (int i = 0; i < limit; i++) {
            temp[i] = stations[i];
        }
        return temp;
    }

    public static void menu(CTAStation[] stations) {
        Scanner input = new Scanner(System.in);
        boolean done = false;

        do {
            System.out.println("1. Display Station Names");
            System.out.println("2. Display Stations with/without Wheelchair access");
            System.out.println("3. Display Nearest Station");
            System.out.println("4. Exit");
            System.out.print("Choice: ");

            String in = input.nextLine();

            switch (in) {
                case "1":
                    displayStationNames(stations);
                    break;
                case "2":
                    displayByWheelchair(input, stations);
                    break;
                case "3":
                    displayNearest(input,stations);
                    break;
                case "4":
                    done = true;
                    break;
                default:
                    System.out.println("Sorry, but I can't understand that...");
            }


        } while (!done);

    }

    public static void displayStationNames(CTAStation[] stations) {
        for (int i = 0; i < stations.length; i++) {
            System.out.println(stations[i].getName());
        }
    }

    public static void displayByWheelchair(Scanner input, CTAStation[] stations) {
        boolean done = false;
        do {
            System.out.print("'y' for stations with wheelchair access, and 'n' for stations without wheelchair access\nEnter y/n: ");
            String in = input.nextLine();
            boolean hasStationWithWheelchair = false;
            boolean hasStationWithoutWheelchair = false;
            switch (in) {
                case "y":
                    for (int i = 0; i < stations.length; i++) {
                        if (stations[i].hasWheelchair()) {
                            System.out.println(stations[i]);
                            hasStationWithWheelchair = true;
                        }
                    }
                    if (!hasStationWithWheelchair) {
                        System.out.println("Stations with wheelchair were not found!");
                    }
                    done = true;
                    break;
                case "n":
                    for (int i = 0; i < stations.length; i++) {
                        if (!stations[i].hasWheelchair()) {
                            System.out.println(stations[i]);
                            hasStationWithoutWheelchair = true;
                        }
                    }
                    if (!hasStationWithoutWheelchair) {
                        System.out.println("Stations without wheelchair were not found!");
                    }
                    done = true;
                    break;
                default:
                    System.out.println("Sorry, but I didn't understand that...");
            }
        } while (!done);
    }

    public static void displayNearest(Scanner input, CTAStation[] stations) {
        double[] distances = new double[stations.length];

        try {
            System.out.println("You need to enter a latitude and longitude.");
            System.out.print("Latitude: ");
            double lat = Double.parseDouble(input.nextLine());
            System.out.print("Longitude: ");
            double lng = Double.parseDouble(input.nextLine());
            for (int i = 0; i < distances.length; i++) {
                distances[i] = stations[i].calcDistance(lat, lng);
            }
        } catch (Exception e) {
            System.out.println("That's not a number.");
        }

        double min = distances[0];
        int minIndex = 0;
        for (int i = 1; i < distances.length; i++) {
            if (distances[i] < min) {
                min = distances[i];
                minIndex = i;
            }
        }

        System.out.println("Nearest station: " + stations[minIndex]);



    }


}
