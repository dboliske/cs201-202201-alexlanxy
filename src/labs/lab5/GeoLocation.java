// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	 Date：Mar. 06, 2022	 	Program Name: GeoLocation.java
package labs.lab5;

public class GeoLocation {
	// create instance variables, lat for latitude, lng for longitude
	private double lat;
	private double lng;
	
	// default constructor
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	// non-default constructor
	public GeoLocation(double lat, double lng) {
		this.lat = 0.0;
		setLat(lat);
		this.lng = 0.0;
		setLng(lng);
	}
	
	// accessor method for lat
	public double getLat() {
		return lat;
	}
	
	// accessor method for lng
	public double getLng() {
		return lng;
	}
	
	// mutator method for lat
	public void setLat(double lat) {
		if (validLat(lat)) {
			this.lat = lat;
		}
	}
	
	// mutator method for lng
	public void setLng(double lng) {
		if (validLng(lng)) {
			this.lng = lng;
		}
	}
	
	// toString method
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	// Validate lat
	public boolean validLat(double lat) {
		return lat >= -90 && lat <= 90;
	}
	
	// validate lng
	public boolean validLng(double lng) {
		return lng >= -180 && lng <= 180;
	}
	
	// compare this instance to another
	public boolean equals(Object obj) {
		if (!(obj instanceof GeoLocation)) {
			return false;
		}
		GeoLocation g = (GeoLocation) obj;
		return lat == g.lat && lng == g.lng;
	}

	// method takes another GeoLocation to calculate the distance
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(this.getLat() - g.getLat(), 2) + Math.pow(this.getLng() - g.getLng(), 2));
	}

	// method takes a lat and a lng to calculate the distance
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.getLat() - lat, 2) + Math.pow(this.getLng() - lng, 2));
	}
	
}



