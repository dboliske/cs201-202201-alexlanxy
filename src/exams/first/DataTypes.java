// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/2/25 	Program Name: DataTypes
package exams.first;

import java.util.Scanner;

public class DataTypes {
    public static void main(String[] args) {
        // create a scanner
        Scanner input = new Scanner(System.in);
        // prompt the user for a number
        System.out.print("Please input an integer: ");
        int in;

        // catch the input exception
        try {
            in = Integer.parseInt(input.nextLine());
        } catch (Exception e) {
            System.out.println("That's not an integer!");
            input.close();
            return;
        }
        input.close();
        char c = (char)(in + 65);   // convert the integer to character
        System.out.println("Character: " + c);  // print the character

    }
}
