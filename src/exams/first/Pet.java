// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/2/25 	Program Name: Pet
package exams.first;

public class Pet {
	// create instance variables
    private String name;
    private int age;

    // default constructor
    public Pet() {
        name = "unnamed";
        age = 1;
    }

    // non-default constructor
    public Pet(String name, int age) {
        this.name = name;
        this.age = 1;
        setAge(age);
    }

    // mutator for name
    public void setName(String name) {
        this.name = name;
    }

    // mutator for age
    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
        }
    }

    // accessor for name
    public String getName() {
        return name;
    }

    // accessor for age
    public int getAge() {
        return age;
    }

    // equals method
    public boolean equals(Object obj) {
        if (!(obj instanceof Pet)){
            return false;
        }
        Pet p = (Pet)obj;
        return this.name.equals(p.getName()) && this.age == p.getAge();
    }

    // toString method
    public String toString() {
        return name + " is " + age + " years old.";
    }
}
