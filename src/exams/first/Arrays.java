// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/2/25 	Program Name: Arrays
package exams.first;

import java.util.Scanner;

public class Arrays {
    public static void main(String[] args) {
        // create a scanner
        Scanner input = new Scanner(System.in);
        String[] words = new String[5];     // store the user input
        String[] toPrint = new String[words.length];   // store the words that appears more than once
        System.out.println("Please input " + words.length + " words. Enter \"exit\" to end the program.");
        for (int i = 0; i < words.length; i++) {
            System.out.print((i+1) + ": ");
            String in = input.nextLine();
            if (in.equals("exit")) {    // user quits midway
                input.close();
                System.out.println("User exited!");
                return;
            }
            words[i] = in;
        }
        input.close();

        int count = 0;  // to count the number of words that appear more than once
        for (int i = 0; i < words.length - 1; i++) {  // compare all the words to each other
            for (int j = i + 1; j < words.length; j++ ) {
                if (words[i].equals(words[j])) {
                    boolean hasExisted = false;     // to avoid the same word being printed, check whether the same word has existed in the "toPrint"
                    for (int m = 0; m < toPrint.length; m++) {
                        if (words[i].equals(toPrint[m])) {
                            hasExisted = true;
                            break;
                        }
                    }
                    if (!hasExisted) {
                        toPrint[count] = words[i];
                        count++;
                    }

                }
            }
        }

        System.out.println("Words that appear more than once: ");
        for (int i = 0; i < count; i++) {       // only print the elements which store words.
            System.out.println(toPrint[i]);
        }



    }
}
