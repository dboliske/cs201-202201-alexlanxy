// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/2/25 	Program Name: Selection
package exams.first;

import java.util.Scanner;

public class Selection {
    public static void main(String[] args) {
        // create a scanner
        Scanner input = new Scanner(System.in);
        // prompt the user for a number
        System.out.print("Please input an integer: ");
        int in;

        // catch the input exception
        try {
            in = Integer.parseInt(input.nextLine());
        } catch (Exception e) {
            System.out.println("That's not an integer!");
            input.close();
            return;
        }
        input.close();
        if (in % 3 == 0 && in % 2 == 0) {
            System.out.println("foobar");   // both divisible
        } else if (in % 2 == 0) {
            System.out.println("foo");      // divisible by 2
        } else if (in % 3 == 0) {
            System.out.println("bar");      // divisible by 3
        }

    }
}
