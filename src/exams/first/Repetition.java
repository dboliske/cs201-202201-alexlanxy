// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/2/25 	Program Name: Repetition
package exams.first;

import java.util.Scanner;

public class Repetition {
    public static void main(String[] args) {
        // create a scanner
        Scanner input = new Scanner(System.in);
        // prompt the user for a number
        System.out.print("Please input an integer: ");
        int in;

        // catch the input exception
        try {
            in = Integer.parseInt(input.nextLine());
        } catch (Exception e) {
            System.out.println("That's not an integer!");
            input.close();
            return;
        }
        input.close();

        for (int row = 0; row < in; row++) {
            for (int col = 0; col < in; col++) {
                if (col >= row) {       // if the column >= row, always "*"
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();       // print a new line
        }
    }
}
