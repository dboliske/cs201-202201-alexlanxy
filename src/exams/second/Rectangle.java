// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name:Rectangle
package exams.second;

public class Rectangle extends Polygon{
    // variables
    private double width;
    private double height;

    // default constructor
    public Rectangle() {
        super();
        width = 1;
        height = 1;
    }

    // mutators
    public void setWidth(double width) {
        if (width > 0) {
            this.width = width;
        }
    }

    public void setHeight(double height) {
        if (height > 0) {
            this.height = height;
        }
    }

    // accessors
    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    // toString method
    @Override
    public String toString() {
        return super.getName() + " is a rectangle, " + "width: " + width + ", " + "height: " + height;
    }

    // calculate area
    @Override
    public double area() {
        return height * width;
    }

    // calculate perimeter
    @Override
    public double perimeter() {
        return 2 * (height + width);
    }
}
