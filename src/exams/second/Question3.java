// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name: find min and max
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class Question3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Double> numbers = new ArrayList<>();
        // prompt the user for numbers
        System.out.println("Input numbers to find the minimum and the maximum. Enter Done to finish.");
        boolean done = false;
        do {
            System.out.print("Number" + (numbers.size() + 1) + ": " );
            String in = input.nextLine();
            if (in.equalsIgnoreCase("done")) {
                done = true;
            } else {
                try {
                    double number = Double.parseDouble(in);
                    numbers.add(number);
                } catch (Exception e) {
                    System.out.println("That's not a number!");
                }
            }
        }while (!done);
        input.close();

        if (numbers.size() != 0) {
            double min = findMin(numbers);
            double max = findMax(numbers);
            System.out.println("Minimum: " + min);
            System.out.println("Maximum: " + max);
        } else {
            System.out.println("You haven't input a number.");
        }
        System.out.println("Goodbye.");
    }

    // method to find the minimum value of an array list.
    public static double findMin(ArrayList<Double> numbers) {
        double min = numbers.get(0);
        for (double number: numbers) {
            if (number < min) {
                min = number;
            }
        }
        return min;
    }

    // method to find the maximum value of an array list.
    public static double findMax(ArrayList<Double> numbers) {
        double max = numbers.get(0);
        for (double number: numbers) {
            if (number > max) {
                max = number;
            }
        }
        return max;
    }
}
