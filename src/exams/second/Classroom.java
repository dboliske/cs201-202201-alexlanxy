// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name: Classroom
package exams.second;

public class Classroom {
    // variables
    protected String building;
    protected String roomNumber;
    private int seats;

    // default constructor
    public Classroom() {
        building = "B";
        roomNumber = "R";
        seats = 2;
    }

    // mutators
    public void setBuilding(String building) {
        this.building = building;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void setSeats(int seats) {
        if (seats > 0) {
            this.seats = seats;
        }
    }

    // accessors
    public String getBuilding() {
        return building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public int getSeats() {
        return seats;
    }

    // toString method
    @Override
    public String toString() {
        return roomNumber + " is in " + building + " Building, " + "and has " + seats + " seats. ";
    }
}
