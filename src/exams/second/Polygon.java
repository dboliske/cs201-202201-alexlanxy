// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name:Polygon
package exams.second;

public abstract class Polygon {
    // variable
    protected String name;

    // default constructor
    public Polygon() {
        name = "unnamed";
    }

    // mutator
    public void setName(String name) {
        this.name = name;
    }

    // accessor
    public String getName() {
        return name;
    }

    // toString method
    @Override
    public String toString() {
        return "Name: " + name;
    }

    public abstract double area();
    public abstract double perimeter();
}
