// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name: Computer Lab
package exams.second;

public class ComputerLab extends Classroom{
    // variable
    private boolean computers;

    // default constructor
    ComputerLab() {
        super();
        computers = true;
    }

    // mutator
    public void setComputers(boolean computers) {
        this.computers = computers;
    }

    // accessor
    public boolean hasComputers() {
        return this.computers;
    }

    // toString method
    @Override
    public String toString() {
        return super.toString() + "It has " + (computers? "": "no ") + "computers.";
    }
}
