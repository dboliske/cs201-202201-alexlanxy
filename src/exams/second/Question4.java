// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name: Selection Sort
package exams.second;

public class Question4 {
    public static void main(String[] args) {
        String[] strings = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
        strings = sort(strings);
        for (String string: strings) {
            System.out.print(string + " ");

        }
    }

    // selection sort method
    public static String[] sort(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            // find the index of min
            int minIndex = i;
            for (int j = i + 1; j < strings.length; j++) {
                if (strings[j].compareTo(strings[minIndex]) < 0) {
                    minIndex = j;
                }
            }
            // if the first is not the minimum, swap the first and the minimum one.
            if (minIndex != i) {
                String temp = strings[i];
                strings[i] = strings[minIndex];
                strings[minIndex] = temp;
            }
        }
        return strings;
    }
}
