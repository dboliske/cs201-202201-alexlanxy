// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name: Circle
package exams.second;

public class Circle extends Polygon{
    // variables
    private double radius;

    // default construction
    public Circle() {
        super();
        radius = 1;
    }

    // mutator
    public void setRadius(double radius) {
        if (radius > 0) {
            this.radius = radius;
        }
    }

    // accessor
    public double getRadius() {
        return radius;
    }

    // toString method
    @Override
    public String toString() {
        return super.getName() + " is a circle, " + "radius: " + radius;
    }

    // calculate area
    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    // calculate perimeter
    @Override
    public double perimeter() {
        return 2 * Math.PI * radius ;
    }
}
