// Name: Xinyu Lan(兰新宇)	Course: CS201 sec.#03	Date：2022/4/29 	Program Name: Jump Search
package exams.second;

import java.util.Scanner;

public class Question5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        while(true) {
            System.out.print("Input a number to search: ");
            try {
                double value = Double.parseDouble(input.nextLine());
                int position = search(numbers, value);
                System.out.println("Position: " + position);
                break;
            } catch (Exception e) {
                System.out.println("That's not a number.");
            }
        }
        input.close();
    }

    // search by numbers and value
    public static int search(double[] numbers, double value) {
        return search(numbers, value, 0, numbers.length - 1);
    }

    // search by numbers and value, needs the index of the start and end
    public static int search(double[] numbers, double value, int start, int end) {
        // those condition will stop the recursion
        if (value < numbers[start] || value > numbers[end]) {
            return -1;
        }
        if ((end - start == 1) && (value > numbers[start] && value < numbers[end])) {
            return -1;
        }
        // calculate the step
        int step = (int)Math.sqrt(end - start);
        int prev = start;
        int later = prev + step;
        // find the interval of the input value
        while (numbers[Math.min(later, end)] < value) {
            prev = later;
            later += step;
            if (prev > end) {
                return -1;
            }
        }
        if (numbers[prev] == value) {
            return prev;
        }
        if (numbers[later] == value) {
            return later;
        }
        // recursively search
        return search(numbers, value, prev, Math.min(later, end));

    }
}
